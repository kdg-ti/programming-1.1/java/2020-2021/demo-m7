package be.kdg.prog1.package_private;

public class DemoClass {
    private int myNumber;

    // 'package private'
    /*public*/ int getMyNumber() {
        return myNumber;
    }

    @Override
    public String toString() {
        return "DemoClass: " + getMyNumber();
    }
}
