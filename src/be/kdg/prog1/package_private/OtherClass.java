package be.kdg.prog1.package_private;

public class OtherClass {
    public static void main(String[] args) {
        DemoClass demoClass = new DemoClass();

        // I can access 'getMyNumber' from here because
        // OtherClass is in the SAME PACKAGE as DemoClass.
        System.out.println("NUMBER: " + demoClass.getMyNumber());
    }
}
