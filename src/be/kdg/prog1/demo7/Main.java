package be.kdg.prog1.demo7;

public class Main {
    public static void main(String[] args) {
        Teacher lars = new Teacher("Lars Willemsens", 40, 123456789L, 12345.0);

        Student student = new Student("1234567-10", 2023);
        student.setAge((short)25);

        lars.print();
        student.print();

        System.out.println("TOSTRING: " + lars);
        System.out.println("TOSTRING: " + student);

        Person person = new Teacher("Jan de Rijke", 40, 123456789L, 12345.0);
        // Not possible:
        //Teacher teacher = new Person();

        // Which print method is going to be called?
        // Person::print or Teacher::print
        person.print(); // ... is calling the most 'specific' method

        final Person person2 = new Student("987654-10", 2030);
        //person2 = new Student("77777777-10", 2050);

        // person??? which type is this variable referring to?
        if (person instanceof Teacher) {
            System.out.println("It's a teacher!");
        } else if (person instanceof Student) {
            System.out.println("It's a student!");
        } else {
            System.out.println("It's NOT a teacher and NOT a student!");
        }

        boolean isItAteacher = person instanceof Teacher;
        System.out.printf("BOOLEAN: " + isItAteacher);
    }
}
