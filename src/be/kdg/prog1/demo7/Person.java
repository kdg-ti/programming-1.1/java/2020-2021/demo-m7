package be.kdg.prog1.demo7;

public class Person { // implicitly extending Object --> inherits toString() ... etc.
    private String name;
    private short age;

    public String getName() {
        return name;
    }

    public short getAge() {
        return age;
    }

    public void setAge(short age) {
        if (age > 0) {
            this.age = age;
        }
        // else: DO NOTHING
    }

    public void print() {
        System.out.printf("%20s %-5d", getName(), getAge());
    }

    @Override // overriding 'toString' of the 'java.lang.Object' class
    public String toString() {
        return "NAME: " + name + "; AGE: " + age;
    }
}
