package be.kdg.prog1.a701;

public class Point3D extends Point{
    private int z;

    public int getZ() {
        return z;
    }

    public Point3D(int x, int y, int z) {
        // Calling another constructor
        super(x, y); // when x and y are private

        //this.x = x; // protected access allows this
        //this.y = y; // protected access allows this
        this.z = z;
    }
}
