package be.kdg.prog1.a701;

public class Point {
    protected int x; //private is still better
    protected int y; //private is still better

    public Point() {
        this(0, 0); // Calling the other constructor
        // Kinda useless since '0' is the default value for
        // int attributes.
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "x: " + x + " y: " + y;
    }
}
